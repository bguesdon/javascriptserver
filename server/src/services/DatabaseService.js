const mysql = require('mysql')
const config = require('../../config')
const color = require('colors')

const db = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    multipleStatements: true
})

db.connect((err) => {
    if (err) {
        console.error('[DatabaseService] Connection to database: FAILED'.red)
        console.error(err)
        process.exit(1)
    }

    console.log('[DatabaseService] Connection to database: SUCCESS'.green)
    db.query(`USE ${config.db.database}`, (err, res) => {
        if (err) {
            console.error(`[DatabaseService] `.white, `No match for ${config.db.database} database`.red)
            console.error(err)
            process.exit(1)
        }
    })
})

module.exports = db