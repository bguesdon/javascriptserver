const jwt = require('jsonwebtoken')

const config = require('../../config')

module.exports = (req, res, next) => {
    // Exceptions for login and register routes
    const exceptRoutes = [
        '/auth/login',
        '/auth/register',
        '/api-docs/'
    ]
    if (exceptRoutes.indexOf(req.originalUrl) >= 0 ||
        (req.originalUrl.split('/')[1] && req.originalUrl.split('/')[1] == 'api-docs')) {
        return next()
    }

    if (!req.headers.authorization) {
        return res
            .status(401)
            .json({
                message: 'No token provided'
            })
    }

    const token = req.headers.authorization.split(' ')[1]
    jwt.verify(token, config.jwt.secret, (err, decoded) => {
        if (err || !decoded) {
            return res
                .status(401)
                .json({
                    message: 'Invalid token'
                })
        }

        next()
    })
}