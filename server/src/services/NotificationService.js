const webpush = require('web-push')

const config = require('../../config')
const UserService = require('./UserService')

let subscriptions = []

const NotificationService = {
    _init(app) {
        webpush.setVapidDetails(
            'mailto:tomchittync@gmail.com',
            config.vapidKeys.public,
            config.vapidKeys.private
        )

        app.post('/subscribe', (req, res) => {
            UserService.getUserUuid(req, (err, uuid) => {
                if (err) {
                    return res
                        .status(401)
                        .json(err.error)
                }

                this.subscribeUser(uuid, req.body, () => {
                    res
                    .status(201)
                    .json({ message: 'Created' })
                })
            })
        })
    },

    subscribeUser(uuid, sub, done) {
        for (let i in subscriptions) {
            if (subscriptions[i] && i == uuid) {
                subscriptions[i] = sub
                return done()
            }
        }

        subscriptions[uuid] = sub
        done()
    },

    getUserSubscription(uuid) {
        for (let i in subscriptions) {
            if (subscriptions[i] && i == uuid) {
                return subscriptions[i]
            }
        }

        return null
    },

    send(uuid, payload) {
        let subscription = this.getUserSubscription(uuid)

        if (subscription) {
            webpush.sendNotification(subscription, payload)
            .catch((e) => {
                console.error(e)
            })
        }
    }
}

module.exports = NotificationService