const db = require('./DatabaseService')
const fs = require('fs')
const mime = require('mime-types')
const moment = require('moment')
const filenamify = require('filenamify')
const { v4: uuidv4 } = require('uuid')
const imageToBase64 = require('image-to-base64')
const { initialize } = require('passport')

module.exports = {
    uploadMethod: Function,
    isImage(file) {
        const allowed = [
            'image/gif',
            'image/jpeg',
            'image/png',
            'image/bmp'
        ]

        if (!allowed.includes(file.mimetype)) {
            return false
        }

        return true
    },

    writeFile(file, done) {
        let fileContents = Buffer.alloc(file.size, file.data, 'base64')
        let ext = mime.extension(file.mimetype)
        let date = moment().format('DD-MM-YYYY_hh-mm-ss')
        let filename = file.name.split('.')[0]
        let path = `${global.__imagesDir}/${filename}_${date}.${ext}`

        if (!fs.existsSync(global.__imagesDir)) {
            fs.mkdirSync(global.__imagesDir, { recursive: true })
        }

        fs.writeFile(path, fileContents, (err) => {
            if (err) {
                return done(err)
            }

            console.log(`File ${path}`.blue, 'saved'.green)

            return done(null, {
                path: path
            })
        })
    },

    base64MimeType(encoded) {
        var result = null;
      
        if (typeof encoded !== 'string') {
          return result;
        }
      
        var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
      
        if (mime && mime.length) {
          result = mime[1];
        }
      
        return result;
    },

    writeFileFromBase64(data, done) {
        let date = moment().format('DD-MM-YYYY_hh-mm-ss')
        let filename = filenamify(data.title)
        let imgMimetype = this.base64MimeType(data.data)
        let ext = mime.extension(imgMimetype) ? mime.extension(imgMimetype) : 'png'
        let path = `${global.__imagesDir}/${filename}_${date}.${ext}`
        let base64Image = data.data.split(';base64,').pop()

        if (!fs.existsSync(global.__imagesDir)) {
            fs.mkdirSync(global.__imagesDir, { recursive: true })
        }

        fs.writeFile(path, base64Image, {encoding: 'base64'}, (err) => {
            if (err) {
                return done(err)
            }

            console.log(`File ${path}`.blue, 'saved'.green)

            return done(null, {
                path: path
            })
        })
    },

    encodeImage(image, done) {
        imageToBase64(image)
        .then((response) => {
            return done(null, response)
        })
        .catch((error) => {
            return done(error)
        })
    },

    encodeImageAsync(image) {
        imageToBase64(image)
        .then((response) => {
            return response
        })
        .catch((err) => {
            return null
        })
    },

    async encodeImages(images, done) {
        let data = []

        if (images.length == 0) {
            return done(null, [])
        }

        for (let i = 0; i < images.length; i++) {
            this.encodeImage(images[i].path, (err, encoded) => {
                if (!err) {
                    data.push({
                        uuid: images[i].uuid,
                        username: images[i].username ? images[i].username : null,
                        title: images[i].title,
                        description: images[i].description,
                        createdAt: images[i].createdAt,
                        author: images[i].author,
                        image: encoded
                    })
                }

                if (i == images.length - 1) {
                    return done(null, data)
                }
            })
        }
    },

    uploadBase64({ body, user }, done) {
        this.writeFileFromBase64({
            title: body.title,
            data: body.file
        }, (err, data) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            const uuid = uuidv4()
            const date = moment().format('YYYY-MM-DD HH:mm:ss')

            db.query(`INSERT INTO images VALUES (?, ?, ?, ?, ?, ?)`, [
                uuid,
                body.title,
                body.description,
                data.path,
                date,
                user
            ], (err, result) => {
                if (err) {
                    try {
                        fs.unlinkSync(data.path)
                        console.log(`File ${data.path}`.blue, 'deleted'.red)
                    } catch (e) {
                        console.error('Cannot delete file'.red, e)
                    }
                    
                    return done({
                        status: 500,
                        error: err
                    })
                }

                return done(null)
            })
        })
    },

    upload({ file, body, user }, done) {
        if (!this.isImage(file)) {
            return done({
                status: 400,
                error: 'Bad request: the file is not an image'
            })
        }

        this.writeFile(file, (err, data) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            const uuid = uuidv4()
            const date = moment().format('YYYY-MM-DD HH:mm:ss')

            db.query(`INSERT INTO images VALUES (?, ?, ?, ?, ?, ?)`, [
                uuid,
                body.title,
                body.description,
                data.path,
                date,
                user
            ], (err, result) => {
                if (err) {
                    try {
                        fs.unlinkSync(data.path)
                        console.log(`File ${data.path}`.blue, 'deleted'.red)
                    } catch (e) {
                        console.error('Cannot delete file'.red, e)
                    }
                    
                    return done({
                        status: 500,
                        error: err
                    })
                }

                return done(null)
            })
        })
    },

    getImages(user, done) {
        db.query('SELECT * FROM images WHERE author = ? ORDER BY createdAt DESC', [user], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            return done(null, result)
        })
    },

    getById(data, done) {
        db.query('SELECT * FROM images WHERE uuid = ? AND author = ?', [data.uuid, data.user], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            return done(null, result[0])
        })
    },

    update(data, done) {
        db.query('SELECT author FROM images WHERE uuid = ?', [data.uuid], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }
            if (result[0].author != data.user) {
                return done({
                    status: 401,
                    error: 'Unauthorized'
                })
            }

            db.query('UPDATE images SET title = ?, description = ? WHERE uuid = ?', [data.title, data.description, data.uuid], (err, result) => {
                if (err) {
                    return done({
                        status: 500,
                        error: err
                    })
                }

                return done(null, result)
            })
        })
    },

    delete(data, done) {
        db.query('SELECT author, path FROM images WHERE uuid = ?', [data.uuid], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }
            if (result[0].author != data.user) {
                return done({
                    status: 401,
                    error: 'Unauthorized'
                })
            }

            const path = result[0].path

            db.query('DELETE FROM images WHERE uuid = ?', [data.uuid], (err, result) => {
                if (err) {
                    return done({
                        status: 500,
                        error: err
                    })
                }

                fs.unlink(path, (err) => {
                    if (err) {
                        return done({
                            status: 500,
                            error: 'Could not delete image file\n' + err
                        })
                    }

                    return done(null)
                })
            })
        })
    }
}