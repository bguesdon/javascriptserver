const db = require('./DatabaseService')
const config = require('../../config')
const ImageService = require('./ImageService')
const UserService = require('./UserService')
const NotificationService = require('./NotificationService')

module.exports = {
    getFriends(data, done) {
        let sql = `
            SELECT u.uuid, u.username, u.email, fr.status, fr.fromUser, fr.toUser FROM users AS u
            INNER JOIN friend_relationships AS fr ON (u.uuid = fr.fromUser OR u.uuid = fr.toUser)
            WHERE (fr.fromUser = "${data.user}" OR fr.toUser = "${data.user}") AND u.uuid != "${data.user}"`

        if (data.status) {
            sql += ` AND fr.status = "${data.status}"`
        }

        db.query(sql, (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            // Check who is the sender
            for (let i = 0; i < result.length; i++) {
                if (result[i].fromUser == data.user) {
                    result[i].sentByMe = true
                } else {
                    result[i].sentByMe = false
                }
                delete result[i].fromUser
                delete result[i].toUser
            }

            return done(null, result)
        })
    },

    getFriend(data, done) {
        this.areFriends(data.user, data.friend, (err, res) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }
            if (!res) {
                return done({
                    status: 401,
                    error: 'Unauthorized: the users are not friends'
                })
            }

            db.query('SELECT uuid, username, email FROM users WHERE uuid = ?', [data.friend], (err, result) => {
                if (err) {
                    return done({
                        status: 500,
                        error: err
                    })
                }
                
                return done(null, result[0])
            })
        })
    },

    addFriend(data, done) {
        db.query('INSERT INTO friend_relationships(fromUser, toUser, status) VALUES(?, ?, ?)', [
            data.user, data.friend, 'pending'
        ], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            UserService.getUserByUuid(data.user, (err, user) => {
                if (err) {
                    return done({
                        status: err.status,
                        error: err.error
                    })
                }

                // Push notification
                NotificationService.send(data.friend, JSON.stringify({
                    title: "Demande d'amis",
                    body: `${user.username} vous a envoyé une demande d'amis`,
                    click_open_url: `${config.client.url}/#/request`
                }))
    
                return done(null, result)
            })
        })
    },

    updateStatus(data, done) {
        this.areFriends(data.user, data.friend, (err, res) => {
            if (err) {
                return done({
                    status: err.status,
                    error: err.error
                })
            }
            if (!res) {
                return done({
                    status: 401,
                    error: 'Unauthorized: users are not friends'
                })
            }

            db.query(`
            UPDATE friend_relationships SET status = ? 
            WHERE (fromUser = ? AND toUser = ?) OR (fromUser = ? AND toUser = ?)
            `, [data.status, data.user, data.friend, data.friend, data.user], (err, result) => {
                if (err) {
                    return done({
                        status: 500,
                        error: err
                    })
                }

                if (data.status == 'accepted') {
                    UserService.getUserByUuid(data.user, (err, user) => {
                        if (err) {
                            return done({
                                status: err.status,
                                error: err.error
                            })
                        }
        
                        // Push notification
                        NotificationService.send(data.friend, JSON.stringify({
                            title: "Demande d'amis",
                            body: `${user.username} a accepté votre demande d'amis`,
                            click_open_url: `${config.client.url}/#/profil/${data.user}`
                        }))
            
                        return done(null, result)
                    })
                } else {
                    return done(null)
                }
            })
        })
    },

    deleteFriend(data, done) {
        db.query(`
        DELETE FROM friend_relationships
        WHERE (fromUser = ? AND toUser = ?) OR (fromUser = ? AND toUser = ?)
        `, [data.user, data.friend, data.friend, data.user], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }
            if (result.affectedRows == 0) {
                return done({
                    status: 404,
                    error: 'No relationship found'
                })
            }
            
            return done(null)
        })
    },

    deleteAllFriends(uuid, done) {
        db.query('DELETE FROM friend_relationships WHERE fromUser = ? OR toUser = ?', [uuid, uuid], (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            return done(null)
        })
    },

    getFriendImages(data, done) {
        this.areAcceptedFriends(data.user, data.friend, (err, res) => {
            if (err) {
                return done({
                    status: err.status,
                    error: err.error
                })
            }
            if (!res) {
                return done({
                    status: 401,
                    error: 'Unauthorized: users are not friends'
                })
            }

            ImageService.getImages(data.friend, (err, images) => {
                if (err) {
                    return done({
                        status: err.status,
                        error: err.error
                    })
                }

                ImageService.encodeImages(images, (err, encoded) => {
                    if (err) {
                        return done({
                            status: 500,
                            error: err
                        })
                    }

                    return done(null, encoded)
                })
            })
        })
    },

    getFriendImage(data, done) {
        this.areAcceptedFriends(data.user, data.friend, (err, res) => {
            if (err) {
                return done({
                    status: err.status,
                    error: err.error
                })
            }
            if (!res) {
                return done({
                    status: 401,
                    error: 'Unauthorized: users are not friends'
                })
            }

            ImageService.getById({
                uuid: data.image,
                user: data.friend
            }, (err, image) => {
                if (err) {
                    return done({
                        status: err.status,
                        error: err.error
                    })
                }

                ImageService.encodeImage(image.path, (err, encoded) => {
                    if (err) {
                        return done({
                            status: 500,
                            error: err
                        })
                    }

                    let result = {
                        uuid: image.uuid,
                        title: image.title,
                        description: image.description,
                        author: image.author,
                        createdAt: image.createdAt,
                        image: encoded
                    }

                    return done(null, result)
                })
            })
        })
    },

    areFriends(uuid1, uuid2, done) {
        db.query(`
            SELECT * FROM friend_relationships 
            WHERE (fromUser = ? AND toUser = ?) OR (fromUser = ? AND toUser = ?)`,
            [uuid1, uuid2, uuid2 ,uuid1],
            (err, result) => {
                if (err) {
                    return done({
                        status: 500,
                        error: err
                    })
                }

                if (result.length == 0) {
                    return done(null, false)
                }
                return done(null, true)
            }
        )
    },

    areAcceptedFriends(uuid1, uuid2, done) {
        db.query(`
            SELECT * FROM friend_relationships 
            WHERE (fromUser = ? AND toUser = ?) OR (fromUser = ? AND toUser = ?)`,
            [uuid1, uuid2, uuid2 ,uuid1],
            (err, result) => {
                if (err) {
                    return done({
                        status: 500,
                        error: err
                    })
                }

                if (result.length == 0) {
                    return done(null, false)
                }
                if (result[0].status != 'accepted') {
                    return done(null, false)
                }
                return done(null, true)
            }
        )
    },

    getLastFriendsImages(data, done) {
        let friends = data.friends

        let condition = ''

        for (let i = 0; i < friends.length; i++) {
            condition += `author = "${friends[i].uuid}"`
            if (i < friends.length - 1) {
                condition += ' OR '
            }
        }

        let sql = `
            SELECT u.username, i.uuid, i.title, i.description, i.path FROM images AS i
            INNER JOIN users AS u ON u.uuid = i.author
            WHERE ${condition} 
            ORDER BY i.createdAt DESC
        `

        db.query(sql, (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            ImageService.encodeImages(result, (err, encoded) => {
                if (err) {
                    return done({
                        status: 500,
                        error: err
                    })
                }

                return done(null, encoded)
            })
        })
    }
}