const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const jwt = require('jsonwebtoken')
const { v4: uuidv4 } = require('uuid')
const bcrypt = require('bcrypt')
const saltRounds = 10

const db = require('./DatabaseService')
const config = require('../../config')

module.exports = {
    _init(app) {
        app.use(passport.initialize())
        app.use(passport.session())

        passport.use(new LocalStrategy((username, password, done) => {
            db.query('SELECT * FROM users WHERE email = ? OR username = ?', [
                username,
                username
            ], (err, result) => {
                if (err) {
                    return done(err)
                }
                if (result.length == 0) {
                    return done(null, false)
                }

                bcrypt.compare(password, result[0].password, (err, res) => {
                    if (err) {
                        return done(err)
                    }
                    if (!res) {
                        return done(null, false)
                    }

                    return done(null, {
                        uuid: result[0].uuid,
                        username: result[0].username,
                        email: result[0].email,
                    })
                })
            })
        }))

        passport.serializeUser((user, done) => {
            done(null, user.uuid);
        })
        
        passport.deserializeUser((uuid, done) => {
            db.query('SELECT * FROM users WHERE uuid = ?', [uuid], (err, res) => {
                done(err, {
                    uuid: res[0].uuid,
                    username: res[0].username,
                    email: res[0].email,
                })
            })
        })
    },

    generateJwt(user, done) {
        jwt.sign({
            uuid: user.uuid,
            username: user.username
        }, config.jwt.secret, { algorithm: 'HS256', expiresIn: 60 * 60 * 24 * 1000 }, (err, token) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            return done(null, {
                token: token
            })
        })
    },

    register(data, done) {
        this.userExists(data, (err, result) => {
            if (err) {
                return done({
                    status: 500,
                    error: err
                })
            }

            if (result) {
                return done({
                    status: 409,
                    error: 'Email or username already exists'
                })
            }

            const uuid = uuidv4()

            bcrypt.hash(data.password, saltRounds, (err, hash) => {
                if (err) {
                    return done({
                        status: 500,
                        error: err
                    })
                }
    
                db.query('INSERT INTO users(uuid, username, email, password) VALUES(?, ?, ?, ?)', [
                    uuid,
                    data.username,
                    data.email,
                    hash
                ], (err, result) => {
                    if (err) {
                        return done({
                            status: 500,
                            error: err
                        })
                    }
    
                    return done(null, result)
                })
            })    
        })
    },

    userExists(data, done) {
        db.query('SELECT * FROM users WHERE email = ? OR username = ?', [ data.email, data.username ], (err, result) => {
            if (err) {
                return done(err)
            }

            if (result.length > 0) {
                return done(null, true)
            }

            return done(null, false)
        })
    }
}