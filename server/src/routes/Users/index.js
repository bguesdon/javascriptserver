const UserService = require('../../services/UserService')
const FriendService = require('../../services/FriendService')

module.exports = {
    getUsers(req, res) {
        UserService.getUsers((err, result) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            return res
                .status(200)
                .json(result)
        })
    },

    getUser(req, res) {
        const uuid = req.params.uuid
        
        if (!uuid) {
            return res
                .status(400)
                .json({
                    message: 'Missing parameter uuid'
                })
        }

        UserService.getUserByUuid(uuid, (err, result) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            return res
                .status(200)
                .json(result)
        })
    },

    deleteUser(req, res) {
        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            UserService.deleteUser(uuid, (err) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }

                FriendService.deleteAllFriends(uuid, (err) => {
                    if (err) {
                        return res
                            .status(err.status)
                            .json({
                                error: err.error
                            })
                    }

                    return res
                        .status(200)
                        .json({
                            message: 'OK'
                        })
                })
            })
        })
    },

    routes(app) {
        app.get('/users', this.getUsers)
        app.get('/users/:uuid', this.getUser)
        app.delete('/users/me', this.deleteUser)
    }
}