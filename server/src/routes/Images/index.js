const mime = require('mime-types')
const imageToBase64 = require('image-to-base64')

const ImageService = require('../../services/ImageService')
const UserService = require('../../services/UserService')

module.exports = {
    getImages(req, res) {
        UserService.getUserUuid(req, (err, user) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            ImageService.getImages(user, (err, images) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }

                ImageService.encodeImages(images, (err, data) => {
                    if (err) {
                        return res
                            .status(500)
                            .json({ error: err })
                    }
    
                    return res
                        .status(200)
                        .json(data)
                })
            })
        })
    },

    uploadImageBase64(req, res) {
        if (!req.body.file || !req.body.title) {
            return res
                .status(400)
                .json({
                    message: 'Bad request: no title or no image provided'
                })
        }

        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            ImageService.uploadBase64({ 
                body: req.body,
                user: uuid
            }, (err) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }

                return res
                    .status(200)
                    .json('OK')
            })
        })        
    },

    uploadImage(req, res) {
        if (!req.files.file || !req.body.title) {
            return res
                .status(400)
                .json({
                    message: 'Bad request: no title or no file provided'
                })
        }
        if (Array.isArray(req.files.file) && req.files.file.length > 1) {
            return res
                .status(400)
                .json({
                    message: 'Bad request: only one image is allowed'
                })
        }

        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            ImageService.upload({
                file: req.files.file,
                body: req.body,
                user: uuid
            }, (err) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }

                return res
                    .status(200)
                    .json('OK')
            })
        })
    },

    getImage(req, res) {
        const uuid = req.params.uuid

        UserService.getUserUuid(req, (err, user) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }
            
            ImageService.getById({
                res, user, uuid
            }, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }

                ImageService.encodeImage(result.path, (err, encoded) => {
                    if (err) {
                        return res
                            .status(500)
                            .json({
                                error: err
                            })
                    }

                    return res.json({
                        uuid: result.uuid,
                        title: result.title,
                        description: result.description,
                        createdAt: result.createdAT,
                        author: result.author,
                        image: encoded
                    })
                })
            })
        })
    },

    updateImage(req, res) {
        const uuid = req.params.uuid

        if (!uuid || !req.body.title || !req.body.description) {
            return res
                .status(400)
                .json({
                    message: 'Bad request: Missing parameter(s)'
                })
        }

        UserService.getUserUuid(req, (err, user) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            ImageService.update({
                uuid: uuid,
                user: user,
                title: req.body.title,
                description: req.body.description,
            }, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }

                return res
                    .status(201)
                    .json({
                        message: 'Edited'
                    })
            })
        })
    },

    deleteImage(req, res) {
        const uuid = req.params.uuid

        if (!uuid) {
            return res
                .status(400)
                .json({
                    message: 'Bad request: Missing parameter uuid'
                })
        }

        UserService.getUserUuid(req, (err, user) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            ImageService.delete({
                uuid: uuid,
                user: user
            }, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({ error: err.error })
                }

                return res
                    .status(200)
                    .json({ message: 'OK' })
            })
        })
    },

    routes(app) {
        app.get('/images', this.getImages)
        app.post('/images', this.uploadImage)
        app.post('/images-base64', this.uploadImageBase64)
        app.get('/images/:uuid', this.getImage)
        app.put('/images/:uuid', this.updateImage)
        app.delete('/images/:uuid', this.deleteImage)
    }
}