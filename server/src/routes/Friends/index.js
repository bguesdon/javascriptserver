const FriendService = require('../../services/FriendService')
const UserService = require('../../services/UserService')

module.exports = {
    getFriends(req, res) {
        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            FriendService.getFriends({
                user: uuid,
                status: req.query.status ? req.query.status : null
            }, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }
    
                return res
                    .status(200)
                    .json(result)
            })
        })
    },

    getFriendsLastImages(req, res) {
        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            FriendService.getFriends({
                user: uuid,
                status: 'accepted'
            }, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }
    
                FriendService.getLastFriendsImages({
                    user: uuid,
                    friends: result
                }, (err, images) => {
                    if (err) {
                        return res
                            .status(err.status)
                            .json({
                                error: err.error
                            })
                    }

                    return res
                        .status(200)
                        .json(images)
                })
            })
        })
    },

    getFriend(req, res) {
        const friendId = req.params.uuid

        if (!friendId) {
            return res
                .status(400)
                .json({
                    message: 'Missing parameter uuid'
                })
        }

        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            FriendService.getFriend({
                user: uuid,
                friend: friendId
            }, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }
    
                return res
                    .status(200)
                    .json(result)
            })
        })
    },

    addFriend(req, res) {
        const friendId = req.params.uuid

        if (!friendId) {
            return res
                .status(400)
                .json({
                    message: 'Missing parameter uuid'
                })
        }

        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            FriendService.addFriend({
                user: uuid,
                friend: friendId
            }, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }
    
                return res
                    .status(201)
                    .json({ message: 'Created' })
            })
        })
    },

    editFriend(req, res) {
        const friendId = req.params.uuid

        if (!friendId || !req.body.status) {
            return res
                .status(400)
                .json({
                    message: 'Bad request: Missing parameter(s)'
                })
        }

        UserService.getUserUuid(req, (err, user) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            FriendService.updateStatus({
                user: user,
                friend: friendId,
                status: req.body.status
            }, (err) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({ error: err.error })
                }

                return res
                    .status(201)
                    .json({ message: 'Edited' })
            })
        })
    },

    deleteFriend(req, res) {
        const friendId = req.params.uuid

        if (!friendId) {
            return res
                .status(400)
                .json({
                    message: 'Missing parameter uuid'
                })
        }

        UserService.getUserUuid(req, (err, uuid) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            FriendService.deleteFriend({
                user: uuid,
                friend: friendId
            }, (err) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            error: err.error
                        })
                }
    
                return res
                    .status(200)
                    .json({ message: 'OK' })
            })
        })
    },

    getFriendImages(req, res) {
        const friendId = req.params.uuid

        if (!friendId) {
            return res
                .status(400)
                .json({
                    message: 'Bad request: Missing parameter uuid'
                })
        }

        UserService.getUserUuid(req, (err, user) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            FriendService.getFriendImages({
                user: user,
                friend: friendId
            }, (err, data) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({ error: err.error })
                }

                return res
                    .status(200)
                    .json(data)
            })
        })
    },

    getFriendImage(req, res) {
        const friendId = req.params.uuid
        const imageId = req.params.imgId

        if (!friendId || !imageId) {
            return res
                .status(400)
                .json({
                    message: 'Bad request: Missing parameter(s)'
                })
        }

        UserService.getUserUuid(req, (err, user) => {
            if (err) {
                return res
                    .status(err.status)
                    .json({
                        error: err.error
                    })
            }

            FriendService.getFriendImage({
                user: user,
                friend: friendId,
                image: imageId
            }, (err, result) => {
                if (err) {
                    return res
                        .status(err.status)
                        .json({
                            message: err.error
                        })
                }

                return res
                    .status(200)
                    .json(result)
            })
        })
    },

    routes(app) {
        app.get('/friends', this.getFriends)
        app.get('/friends/last-images', this.getFriendsLastImages)
        app.get('/friends/:uuid', this.getFriend)
        app.post('/friends/:uuid', this.addFriend)
        app.put('/friends/:uuid', this.editFriend)
        app.delete('/friends/:uuid', this.deleteFriend)
        app.get('/friends/:uuid/images', this.getFriendImages)
        app.get('/friends/:uuid/images/:imgId', this.getFriendImage)
    }
}