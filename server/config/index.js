require('dotenv').config()

const server = {
    protocol: 'https',
    host: 'localhost',
    port: 3000
}

const clientProd = {
    protocol: 'https',
    host: 'chittync.me',
    port: 80
}

const clientDev = {
    protocol: 'http',
    host: 'localhost',
    port: 8080
}

const db = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
}

const jwt = {
    secret: process.env.JWT_SECRET
}

const vapidKeys = {
    public: 'BD-53NyeNKPl0NVSR4lQK9G3ItE8PlXhPhuD7-M7E4UybfskvBEDfW5o6gMnvsNdpDj9MKCsv_iyKtllmiSi_oo',
    private: 'Txi3Mp2Vv66vzUOeSZ7VPCRMokuqWMv20wntvy2-zEk'
}

module.exports = (function () {
    let client

    if (process.env.NODE_ENV == 'production') {
        client = {
            protocol: `${clientProd.protocol}`,
            host: `${clientProd.host}`,
            port: `${clientProd.port}`,
            url: `${clientProd.protocol}://${clientProd.host}`
        }
    } else {
        client = {
            protocol: `${clientDev.protocol}`,
            host: `${clientDev.host}`,
            port: `${clientDev.port}`,
            url: `${clientDev.protocol}://${clientDev.host}:${clientDev.port}`
        }
    }

    return {
        client: client,
        server: {
            protocol: `${server.protocol}`,
            host: `${server.host}`,
            port: `${server.port}`,
            url: `${server.protocol}://${server.host}:${server.port}`
        },
        db: db,
        jwt: jwt,
        vapidKeys: vapidKeys
    }
})()
